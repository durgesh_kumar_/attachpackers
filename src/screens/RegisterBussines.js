import React, { Component } from 'react';
import {
    View,
    Dimensions,
    ScrollView,
    ImageBackground,
    TouchableWithoutFeedback,
    Keyboard,
    KeyboardAvoidingView
} from 'react-native';
import Header from '../components/Header';
import { BoldText, LightText, TextInputField } from '../components/styledTexts';
import { Button, Checkbox } from '../components/button';
import colors from '../styles/colors';

const { height, width } = Dimensions.get('window');

export default class RegisterBussiness extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <ImageBackground
                    source={require('../assets/bg1.png')}
                    style={{ flex: 1, width: width }}
                    resizeMode={'cover'}
                >
                    <View style={{ flex: 1 }}>
                        <View style={{ height: 60 }}>
                            <Header
                                leftNavigation={this.props.navigation}
                                value={'Register Your Business'}
                                backgroundColor={'transparent'}
                                color={'#fff'}
                            />
                        </View>
                        <ScrollView style={{ flex: 1, paddingHorizontal: 20, paddingVertical: 20 }}>
                        <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
            <KeyboardAvoidingView contentContainerStyle={{ flex: 1 }}
              style={{ height: height, width: width-40 }}
              behavior='position' enabled
              keyboardVerticalOffset={-200}
            >
              
              <View style={{ backgroundColor:'rgba(63,68,64,.5)', borderRadius: 10, paddingVertical: 20, paddingHorizontal:20 }}>
              <View style={{justifyContent:'center' }}>
                  <TextInputField
                    containerStyle={{ backgroundColor: 'rgba(255, 255, 255, 0.52)' }}
                    placeholder={'Company Name'}
                  />
                  <TextInputField
                      containerStyle={{ marginTop:20, backgroundColor: 'rgba(255, 255, 255, 0.52)'}}
                      placeholder={'Full Name'}
                  />
                  <TextInputField
                      containerStyle={{ marginTop:20, backgroundColor: 'rgba(255, 255, 255, 0.52)'}}
                      placeholder={'Mobile Number'}
                  />
                  <TextInputField
                      containerStyle={{ marginTop:20, backgroundColor: 'rgba(255, 255, 255, 0.52)'}}
                      placeholder={'Email Address'}
                      secureTextEntry={true}
                  />
                   <TextInputField
                      containerStyle={{ marginTop:20, backgroundColor: 'rgba(255, 255, 255, 0.52)'}}
                      placeholder={'Your City'}
                      secureTextEntry={true}
                  />
                   <TextInputField
                      containerStyle={{ marginTop:20, backgroundColor: 'rgba(255, 255, 255, 0.52)'}}
                      placeholder={'Office Address'}
                      secureTextEntry={true}
                  />
                  <View style={{ marginTop: 20}}>
                  <LightText style={{ color:'#fff'}} >Business Type</LightText>
                    <View style={{ flexDirection:'row', }}>
                    <View style={{ flex: 1, alignItems:'flex-start'}}>
                        <Checkbox
                            value={'Househeld Leads'}
                        />
                        </View>
                        <View style={{ flex: 1, alignItems:'flex-start'}}>
                        <Checkbox
                            value={'Car Leads'}
                        />
                        </View>
                    </View>
                    <View style={{ flexDirection:'row', }}>
                    <View style={{ flex: 1, alignItems:'flex-start'}}>
                        <Checkbox
                            value={'Bike Leads'}
                        />
                        </View>
                        <View style={{ flex: 1, alignItems:'flex-start'}}>
                        <Checkbox
                            value={'Office Leads'}
                        />
                        </View>
                    </View>
                  </View>
              </View>
              <View style={{ paddingVertical: 20 }}>
                      <Button
                        color={colors.buttonColor}
                        value={'REGISTER'}
                        // onPress={()=> this.props.navigation.navigate('drawerNavigation')}
                    />
                    <LightText style={{ textAlign:'center', color:'#fff'}} onPress={()=> this.props.navigation.goBack()} >Back</LightText>
              </View>
              </View>
            </KeyboardAvoidingView>
          </TouchableWithoutFeedback>
                        </ScrollView>
                    </View>
                </ImageBackground>
            </View>
        )
    }
}


