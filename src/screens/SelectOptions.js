import React, {Component} from 'react';
import{
 View,
 Image,
 Dimensions,
 ImageBackground
}from 'react-native';
import Header from '../components/Header';
import { BoldText, LightText } from '../components/styledTexts';
import { Button } from '../components/button';
import colors from '../styles/colors';

const {height, width} = Dimensions.get('window');

export default class SelectOptions extends Component {
    constructor(props){
        super(props);
    }

    componentDidMount(){ 
    }

    render(){
        return(
            <View style={{ flex: 1 }}>
            <ImageBackground
              source={require('../assets/bg1.png')}
              style={{ flex: 1, width: width }}
              resizeMode={'cover'}
            >
              <View style={{ flex: 1 }}>
                <View style={{ flex: .3, alignItems: 'center', justifyContent: 'center' }}>
                    <BoldText style={{ color:'#fff'}}>Logistics</BoldText>
                </View>
                <View style={{ flex: 1, justifyContent: 'center', }}>
                  <Button
                    color={colors.buttonColor}
                    value={'Login'}
                    // onPress={() => this.props.navigation.navigate('loginNavigationOptions')}
                  />
                  <Button
                    color={colors.buttonColor}
                    value={'REGISTER YOUR BUSINESS'}
                     onPress={() => this.props.navigation.navigate('RegisterBussiness')}
                  />
                  <Button
                    color={colors.buttonColor}
                    value={'GET FREE QUOTE'}
                     onPress={() => this.props.navigation.navigate('GetQuote')}
                  />
                </View>
                <View style={{ justifyContent: 'center' }}>
                  <BoldText style={{ textAlign: 'center', color: '#fff', }}>Welcome</BoldText>
                  <BoldText style={{ textAlign: 'center', color: colors.buttonColor, marginVertical: 20 }}>Worldwide freight services</BoldText>
                </View>
              </View>
            </ImageBackground>
          </View>
        )
    }
}


