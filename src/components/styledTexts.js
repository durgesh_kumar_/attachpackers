import React, {Component} from 'react';
import {
    Text,
    View,
    Image,
    TextInput,
    TouchableOpacity
} from 'react-native';

export class BoldText extends Component {
    render() {
        let {
            style
        } = this.props;
        if (style == null || style === undefined) {
            style = {}
        }
        return (<Text style={[{fontWeight: 'bold', color: '#212224', fontSize: 20} ,style]} numberOfLines={this.props.numberOfLines ? this.props.numberOfLines : null}>{this.props.children}</Text>);
    }
}

export class LightText extends Component {
    render() {
        let {
            style
        } = this.props;
        if (style == null || style === undefined) {
            style = {}
        }
        return (<Text style={[{color: '#585858'}, style]} onPress={this.props.onPress}
                      numberOfLines={this.props.numberOfLines ? this.props.numberOfLines : null}>{this.props.children}</Text>);
    }
}

export class TextInputField extends Component {
    state={
        showPassowrd: false
    }
    componentDidMount(){
        this.setState({ showPassowrd: this.props.secureTextEntry })
    }
    render(){
        let{
            style,
            containerStyle
        } = this.props;
        if(style == null || style == undefined) {
            style = {}
        }
        if( containerStyle == null || containerStyle == undefined){
            containerStyle = {}
        }
        return(
            <View style={[{ height: 40, borderRadius: 5, flexDirection: 'row', alignItems: 'center',  backgroundColor: '#fff' }, containerStyle]}>
                {(this.props.image) ? (
                    <Image
                        source = {this.props.image}
                        style={{ height: 30, width:30, marginHorizontal:3}}
                        resizeMode={'contain'}
                    />
                ) : (null)}
                
                <TextInput
                    style={[{ flex: 1, height: 40, marginLeft: 5  }, this.props.style]}
                    secureTextEntry = {this.state.showPassowrd}
                    value={this.props.value}
                    editable={this.props.editable}
                    maxLength={this.props.maxLength}
                    placeholder={this.props.placeholder}
                    keyboardType={this.props.keyboardType}
                    onChangeText={this.props.onChangeText}
                    fontSize={16}
                />
                {(this.props.rightImage) ? (
                    <TouchableOpacity 
                        onPress={()=> this.setState({ showPassowrd: !this.state.showPassowrd})}
                    >
                    {/* <Image
                        source = {(this.state.showPassowrd) ? require('../assets/password.png') : require('../assets/eye-slash.png')}
                        style={{ tintColor: colors.appColor}}
                    /> */}
                    </TouchableOpacity>
                ) : (null)}
            </View>
        )
    }
}