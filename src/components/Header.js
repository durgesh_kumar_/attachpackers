import React from 'react';
import {
    View,
    TouchableOpacity,
    Image,
    Text
} from 'react-native';

export default Header = (props) => {
    return (
        <View style={{flex:1, backgroundColor: props.backgroundColor ? props.backgroundColor : '#fff' }}>
          <View style={{ flex:1,marginTop: 11, flexDirection:'row', alignItems:'center', justifyContent:'center', backgroundColor: props.backgroundColor ? props.backgroundColor : '#fff'}}>
            <TouchableOpacity
              style={{flex: 0.5}}
              onPress={() => {
                  props.leftNavigation.goBack(null);
              }}
            >
              <Image style={{height: 25, width: 25, marginLeft: 3, tintColor: props.color}}
                      resizeMode='contain'
                      source={require('../assets/back.png')}
              />
            </TouchableOpacity>
            <View style={{ flex: 1,  alignItems:'center', justifyContent:'center'}}>
            <Text style={{color: props.color, fontSize: 18 }}>{props.value}</Text>
            </View>
          <View style={{flex: 0.5}}/>
          </View>
        <View style={{marginTop: 0, height: 1, backgroundColor: '#fff'}}/>
        </View>
    )
}