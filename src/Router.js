import React from "react";
import {Image} from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { createStackNavigator } from 'react-navigation-stack';
import Home from './screens/Home';
import SelectOptions from './screens/SelectOptions';
import GetQuote from './screens/GetQuote';
import RegisterBussiness from './screens/RegisterBussines';


export const homeNavigationOptions = createStackNavigator(
    {
        Home:{
            screen: Home
        } 
    },
    {
        headerMode: 'none'
    }
);


export const TabNavigator = createBottomTabNavigator(
    {
        HOME:{
            screen: homeNavigationOptions,
            navigationOptions:{
                tabBarIcon: ({tintColor}) =>{
                    return(
                        <Image
                        source = {require('./assets/Bottom_Icons/Home.png')}
                        style = {{ height: 25, width: 25,tintColor:tintColor}}
                        />
                    )
                }
            }
        },
        
        SOCIAL:{
            screen: homeNavigationOptions,
            navigationOptions:{
                tabBarIcon: ({tintColor}) =>{
                    return(
                        <Image
                        source = {require('./assets/Bottom_Icons/Notifications.png')}
                        style = {{ height: 25, width: 25,tintColor:tintColor}}
                        />
                    )
                }
            }
        }
     },
    {
        initialRouteName: 'HOME',        
        tabBarOptions: {
            activeTintColor: "#fff",
            inactiveTintColor: "#444444",
            showLabel: true,
            showIcon: true,
            tabBarPosition: 'bottom',
            labelStyle: {
                fontSize: 12,
            },
            iconStyle:{
                width: 30,
                height: 30
            },
            style: {
                backgroundColor: 'rgb(236,161,61)',
                alignItems: 'center',
                justifyContent: 'center',
                alignSelf: 'center',
            },
            lazy: true,
            indicatorStyle: '#fff',
        }
    }
);

export const appNavigationOptions = createStackNavigator(
    {
        SelectOptions: { screen: SelectOptions},
        GetQuote: { screen: GetQuote},
        RegisterBussiness: { screen: RegisterBussiness },
        TabNavigator:{ screen: TabNavigator }
    },
    {
        headerMode: 'none'
    }
  );
  
  
  export const AppContainer = createAppContainer(appNavigationOptions);
  
  