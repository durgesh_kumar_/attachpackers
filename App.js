/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {
  View,
  StatusBar,
  Image,
  StyleSheet,
  Dimensions
} from 'react-native';
import AppIntroSlider from 'react-native-app-intro-slider';
import AsyncStorage from '@react-native-community/async-storage';
import { AppContainer } from './src/Router';

const {height, width} = Dimensions.get('screen')

export default class App extends Component {

  constructor(props){
    super(props);
    this.state = {
      showRealApp: false,
      isChecked: false
    }
  }
async componentDidMount(){
    console.disableYellowBox = true;
    // const value = await AsyncStorage.getItem('@notFirstRun')
    AsyncStorage.getItem('@notFirstRun').then((value) =>{
      this.setState({isChecked: true})
    if(value === 'ok') {
      this.setState({ showRealApp:true})
    }
  })
  }
  
  _onDone = () => {
    AsyncStorage.setItem('@notFirstRun', 'ok')
    this.setState({ showRealApp: true });
  }
  _onSkip = () => {
    AsyncStorage.setItem('@notFirstRun', 'ok')
    this.setState({ showRealApp: true });
  };

  render() {
    if(this.state.isChecked){
    if (this.state.showRealApp) {
      return (
        // <SafeAreaView style={{flex: 1, backgroundColor:'#22bcb5'}}>
        <View style={{ flex: 1}}>
          <StatusBar backgroundColor="silver"/>
          <AppContainer />
          </View>
        // </SafeAreaView>
      );
    } else {
      return (
        <AppIntroSlider
          slides={slides}
          onDone={this._onDone}
          showSkipButton={true}
          onSkip={this._onSkip}
          activeDotStyle={{backgroundColor: 'gray'}}
          buttonTextStyle={{color:'gray'}}
        />
      )
    }}else{
      return (
        <View style={{ flex:1}}>
           {/* <Image
            source={require('./src/assets/LoadingPage.png')}
            style={{height: height, width: width}}
            resizeMode={'cover'}
           /> */}
        </View>
      )
    }

  }
};

const styles = StyleSheet.create({
  image: {
    width: 200,
    height: 200,
  },
  text: {
    color: 'gray',
    fontSize: 20,
  },
  title: {
    fontSize: 28,
    fontWeight: 'bold',
    color: 'gray',
    backgroundColor: 'transparent',
    textAlign: 'center',
    marginTop: 16,
  },
});
 
const slides = [
  {
    key: 's1',
    text: 'Stay updated with the event buzz with live update in the form of polls, discussions, pictures and videos shared on the event feed.',
    title: 'Stay updated',
    titleStyle: styles.title,
    textStyle: styles.text,
    image: require('./src/assets/Stay_updated.png'),
    imageStyle: styles.image,
    backgroundColor: '#fff',
  },
  {
    key: 's2',
    title: 'Connect:',
    titleStyle: styles.title,
    textStyle: styles.text,
    text: 'Connect with relevant attendees, view individual attendee profiles, chat or schedule meetings with fellow community members. Use filters to find selective attendee profiles.',
    image: require('./src/assets/connect.png'),
    imageStyle: styles.image,
    backgroundColor: '#fff',
  },
  {
    key: 's3',
    title: 'Event schedule',
    titleStyle: styles.title,
    textStyle: styles.text,
    text: 'Access event schedule through the app, bookmark sessions of interest, set session reminders and create your individual event agenda on the go.',
    image: require('./src/assets/Event-schedule.png'),
    imageStyle: styles.image,
    backgroundColor: '#fff',
  },
  {
    key: 's4',
    title: 'Stay notified',
    titleStyle: styles.title,
    textStyle: styles.text,
    text: ' Receive importent updates on the event, community activites and interactions in the form of push notifications on the app itself.',
    image: require('./src/assets/Stay_notified.png'),
    imageStyle: styles.image,
    backgroundColor: '#fff',
  }
];